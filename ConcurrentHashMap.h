#pragma once
#include <array>
#include <mutex>
#include <list>
#include <algorithm>

namespace internal
{
    template<typename HashType, typename ItemType>
    class ListItem
    {
    public:
        ListItem(HashType hash, ItemType&& item)
            : m_hash(hash)
            , m_item(std::move(item))
            , m_isErased(false)
        {
        }

        ItemType& GetItem()
        {
            return m_item;
        }

        const HashType& GetHash() const
        {
            return m_hash;
        }

        void SetErased(bool isErased)
        {
            m_isErased = isErased;
        }

        void SetItem(HashType hash, ItemType&& item)
        {
            m_hash = hash;
            m_item = std::move(item);
            m_isErased = false;
        }

        bool IsErased() const
        {
            return m_isErased;
        }

        bool operator==(ListItem& other) const
        {
            return this->GetHash() == other.GetHash();
        }

        bool operator<(ListItem& other) const
        {
            return this->GetHash() < other.GetHash();
        }

        auto& GetMutex()
        {
            return m_mutex;
        }

    private:
        std::recursive_mutex m_mutex;
        HashType m_hash;
        ItemType m_item;
        bool m_isErased;
    };
}

template<typename KeyTypeInternal, typename ItemTypeInternal, size_t BucketsCount = 100>
class ConcurrentHashMap
{
public:
    typedef KeyTypeInternal KeyType;
    typedef ItemTypeInternal ItemType;
    typedef std::hash<KeyType> KeyHashType;
    typedef typename std::hash<KeyType>::result_type KeyHashResultType;
    typedef ConcurrentHashMap<KeyType, ItemType, BucketsCount> ContainerType;

    template<typename MapType>
    class IteratorInternal
    {
    public:
        class internal_item
        {
        public:
            internal_item(typename MapType::ListItemTypePtr item)
                : m_item(item)
                , m_lockGuard(item->GetMutex())
            {
            }

            typename MapType::ItemType& get()
            {
                return m_item->GetItem();
            }

        private:
            typename MapType::ListItemTypePtr m_item;
            std::lock_guard<std::recursive_mutex> m_lockGuard;

        };
        typedef std::shared_ptr<internal_item> internal_item_ptr;

    public:
        IteratorInternal(MapType& map, typename MapType::KeyHashResultType itemHash)
            : m_map(map)
            , m_itemHash(itemHash)
        {
        }

        internal_item_ptr operator*()
        {
            auto item = m_map.QueryHash(m_itemHash);
            return std::make_shared<internal_item>(item);
        }

        internal_item_ptr operator->()
        {
            return this->operator *();
        }

        IteratorInternal& operator++()
        {
            m_itemHash = m_map.GetNextItemHash(m_itemHash);
            return *this;
        }

        bool operator==(const IteratorInternal& other)
        {
            return m_itemHash == other.m_itemHash;
        }

        bool operator!=(const IteratorInternal& other)
        {
            return m_itemHash != other.m_itemHash;
        }

    private:
        MapType& m_map;
        typename MapType::KeyHashResultType m_itemHash;
    };

    typedef IteratorInternal<ContainerType> Iterator;

private:
    typedef std::lock_guard<std::mutex> LockGuardType;
    typedef internal::ListItem<KeyHashResultType, ItemType> ListItemType;
    typedef std::shared_ptr<ListItemType> ListItemTypePtr;
    typedef std::list<ListItemTypePtr> BucketType;

    typedef std::array<std::mutex, BucketsCount> MutexArrayType;
    typedef std::array<BucketType, BucketsCount> BucketArrayType;
    typedef size_t PosType;
    typedef std::pair<KeyHashResultType, size_t> ItemInfoType;

    const static KeyHashResultType s_endHash = 0;

public:
    ConcurrentHashMap()
    {
    }

    void Register(KeyType key, ItemType&& item)
    {
        auto itemInfo = GetItemInfo(key);
        LockGuardType lock(m_mutexArray[itemInfo.second]);
        BucketType& bucket = m_buckets[itemInfo.second];
        auto iter = QueryImpl(itemInfo.first, bucket);
        if (iter != bucket.end())
        {
            (*iter)->SetItem(itemInfo.first, std::move(item));
        }
        else
        {
            iter = FindFirstErasedItem(bucket);
            if (iter != bucket.end())
            {
                (*iter)->SetErased(false);
                (*iter)->SetItem(itemInfo.first, std::move(item));
            }
            else
            {
                bucket.insert(bucket.begin(), std::make_shared<ListItemType>(itemInfo.first, std::move(item)));
            }
        }
    }

    void Unregister(KeyType key)
    {
        auto itemInfo = GetItemInfo(key);
        LockGuardType lock(m_mutexArray[itemInfo.second]);
        BucketType& bucket = m_buckets[itemInfo.second];
        auto iter = QueryImpl(itemInfo.first, bucket);
        if (iter != bucket.end())
        {
            (*iter)->SetErased(true);
        }
    }

    Iterator Query(KeyType key)
    {
        auto itemInfo = GetItemInfo(key);
        return Iterator(*this, itemInfo.first);
    }

    size_t GetMaxBucketsCount() const
    {
        return BucketsCount;
    }

    Iterator Begin()
    {
        return Iterator(*this, GetFirstNonEmptyItemHash());
    }

    Iterator End()
    {
        return Iterator(*this, s_endHash);
    }

private:
    friend class IteratorInternal<ContainerType>;

    typename BucketType::iterator FindFirstErasedItem(BucketType& bucket)
    {
        return std::find_if(bucket.begin(),
                            bucket.end(),
                            [](auto item)
                            {
                                return item->IsErased();
                            });
    }

    typename BucketType::iterator FindFirstNonErasedItem(auto from, BucketType& bucket)
    {
        return std::find_if(from,
                            bucket.end(),
                            [](auto& item)
                            {
                                return !item->IsErased();
                            });
    }

    PosType GetBucketPosition(KeyHashResultType hashResult)
    {
        return hashResult % GetMaxBucketsCount();
    }

    ItemInfoType GetItemInfo(KeyType key)
    {
        auto hash = KeyHashType()(++key);
        auto realPos = GetBucketPosition(hash);
        return std::make_pair(hash, realPos);
    }

    ListItemTypePtr QueryHash(KeyHashResultType hash)
    {
        auto pos = GetBucketPosition(hash);
        LockGuardType lock(m_mutexArray[pos]);
        BucketType& bucket = m_buckets[pos];
        auto iter = QueryImpl(hash, bucket);
        if (iter == bucket.end())
        {
            throw std::runtime_error("Item doesn't exist");
        }
        return *QueryImpl(hash, bucket);
    }

    auto QueryImpl(KeyHashResultType hash, BucketType& bucket)
    {
        auto iter = std::find_if(bucket.begin(), bucket.end(), [&](auto& item)
        {
            return item->GetHash() == hash && !item->IsErased();
        });
        if (iter == bucket.end())
        {
            return bucket.end();
        }
        return iter;
    }

    KeyHashResultType GetFirstNonEmptyItemHash()
    {
        for (auto i = 0; i < m_buckets.size(); ++i)
        {
            LockGuardType lock(m_mutexArray[i]);
            BucketType& bucket = m_buckets[i];
            auto iter = FindFirstNonErasedItem(bucket.begin(), bucket);
            if (iter != bucket.end())
            {
                return (*iter)->GetHash();
            }
        }
        return s_endHash;
    }

    KeyHashResultType GetNextItemHash(KeyHashResultType hash)
    {
        auto pos = GetBucketPosition(hash);
        for (auto i = pos; i < m_buckets.size(); ++i)
        {
            LockGuardType lock(m_mutexArray[i]);
            BucketType& bucket = m_buckets[i];
            auto iter = bucket.begin();
            if (i == pos)
            {
                iter = std::find_if(bucket.begin(), bucket.end(), [&hash](auto item)
                {
                    return item->GetHash() == hash;
                });
            }
            iter = FindFirstNonErasedItem(++iter, bucket);
            if (iter != bucket.end())
            {
                return (*iter)->GetHash();
            }
        }
        return s_endHash;
    }

private:
    MutexArrayType m_mutexArray;
    BucketArrayType m_buckets;
};

