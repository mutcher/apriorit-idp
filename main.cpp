#include <thread>
#include <functional>
#include <iostream>
#include <atomic>

#include "ConcurrentHashMap.h"

struct item
{
    int id;
};

int main(int argc, char *argv[])
{
    std::atomic_bool isWorking(true);
    ConcurrentHashMap<int, std::unique_ptr<item>> map;

    auto threadWorker = [&]()
    {
        while(isWorking)
        {
            std::cout << "+new it" << std::endl;
            for(auto iter = map.Begin(); iter != map.End(); ++iter)
            {
                std::cout << (*iter).get()->get()->id << std::endl;
            }
            std::cout << "-new it" << std::endl;
        }
    };
    std::thread tr(threadWorker);

    /*for (int x = 0; x < 1000; ++x)
    {*/

        for (int i = 0; i < 1000; ++i)
        {
            std::unique_ptr<item> tmpItem(new item());
            tmpItem->id = i;
            map.Register(i, std::move(tmpItem));
        }

        for (int i = 0; i < 1000; ++i)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
            map.Unregister(i);
        }
    //}
    isWorking = false;
    if (tr.joinable())
    {
        tr.join();
    }

    return 0;
}
